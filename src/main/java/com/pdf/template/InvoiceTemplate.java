package com.pdf.template;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.pdf.constants.CompanyConst;
import com.pdf.constants.FooterConst;
import com.pdf.constants.InvoiceConst;
import com.pdf.constants.LogoConst;
import com.pdf.constants.TableConst;
import com.pdf.constants.general.ColorConst;
import com.pdf.constants.general.FontConst;
import com.pdf.constants.general.GeneralConst;
import com.pdf.dto.CompanyData;
import com.pdf.dto.FooterData;
import com.pdf.dto.InvoiceData;
import com.pdf.dto.LogoData;
import com.pdf.dto.TableData;
import com.pdfjet.A4;
import com.pdfjet.Align;
import com.pdfjet.Border;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Line;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Paragraph;
import com.pdfjet.Table;
import com.pdfjet.Text;
import com.pdfjet.TextLine;

public class InvoiceTemplate {

	public void generatePdf(String pdfName) throws Exception {
		PDF pdf = new PDF(new BufferedOutputStream(new FileOutputStream(pdfName
				+ ".pdf")));

		Page page = new Page(pdf, A4.PORTRAIT);

		drawDraft(pdf, page);
		drawInvoiceText(pdf, page);
		drawGreyLine(page);
		drawCompanyDetails(pdf, page);
		drawInvoiceDetails(pdf, page);
		drawCompanyPostDetails(pdf, page);
		drawLogo(pdf, page);
		drawTable(pdf, page);
		drawFooter(pdf, page);

		pdf.close();

	}

	private void drawDraft(PDF pdf, Page page) throws Exception {

		Font f1 = new Font(pdf, CoreFont.TIMES_BOLD);
		f1.setSize(120f);

		TextLine text = new TextLine(f1);
		text.setLocation(GeneralConst.DRAFT_X_LOCATION, GeneralConst.DRAFT_Y_LOCATION);
		text.setColor(ColorConst.DRAFT_COLOR);

		text.setTextDirection(GeneralConst.DRAFT_ROTATION);
		text.setText("DRAFT");
		text.drawOn(page);

	}

	private void drawFooter(PDF pdf, Page page) throws Exception {
		Font regularFont = new Font(pdf, FontConst.REGULAR);
		regularFont.setSize(FooterConst.FONT_SIZE);

		Paragraph invoiceTitle = new Paragraph().add(new TextLine(regularFont,
				FooterData.FOOTER_TEXT));
		invoiceTitle.setAlignment(Align.CENTER);

		Text column = new Text(Arrays.asList(invoiceTitle));
		column.setLocation(FooterConst.X_LOCATION, FooterConst.Y_LOCATION);

		column.setWidth(FooterConst.WIDTH);

		column.drawOn(page);

	}

	private void drawInvoiceText(PDF pdf, Page page) throws Exception {

		Font boldFont = new Font(pdf, FontConst.BOLD);
		boldFont.setSize(InvoiceConst.FONT_SIZE);

		Paragraph invoiceTitle = new Paragraph().add(new TextLine(boldFont,
				InvoiceData.INVOICE_TITLE).setColor(ColorConst.GREY_COLOR));
		invoiceTitle.setAlignment(Align.LEFT);

		Text column = new Text(Arrays.asList(invoiceTitle));
		column.setParagraphLeading(InvoiceConst.PARAGRAPH_LEADING);
		column.setLocation(InvoiceConst.X_LOCATION, InvoiceConst.Y_LOCATION);

		column.setWidth(InvoiceConst.WIDTH);

		column.drawOn(page);

	}

	private void drawGreyLine(Page page) throws Exception {
		Line line = new Line(GeneralConst.HORIZONTAL_LINE_X1,
				GeneralConst.HORIZONTAL_LINE_Y1,
				GeneralConst.HORIZONTAL_LINE_X2,
				GeneralConst.HORIZONTAL_LINE_Y2);
		line.setWidth(GeneralConst.HORIZONTAL_LINE_WIDTH);
		line.setColor(ColorConst.GREY_COLOR);
		line.drawOn(page);
	}

	private void drawCompanyPostDetails(PDF pdf, Page page) throws Exception {

		Font regularFont = new Font(pdf, FontConst.REGULAR);
		regularFont.setSize(CompanyConst.POST_FONT_SIZE);

		Paragraph registeredCompanyName = new Paragraph();
		registeredCompanyName.setAlignment(Align.LEFT);
		registeredCompanyName.add(new TextLine(regularFont,
				CompanyData.COMPANY_INC));

		Paragraph companyAddress = new Paragraph();
		companyAddress.setAlignment(Align.LEFT);
		companyAddress.add(new TextLine(regularFont,
				CompanyData.COMPANY_ADDRESS));

		Text column = new Text(Arrays.asList(registeredCompanyName,
				companyAddress));
		column.setParagraphLeading(CompanyConst.POST_PARAGRAPH_LEADING);

		column.setLocation(CompanyConst.POST_X_LOCATION,
				CompanyConst.POST_Y_LOCATION);
		column.setWidth(CompanyConst.POST_WIDTH);

		column.drawOn(page);
	}

	private void drawCompanyDetails(PDF pdf, Page page) throws Exception {

		Font boldFont = new Font(pdf, FontConst.BOLD);
		boldFont.setSize(10f);

		Font regularFont = new Font(pdf, FontConst.REGULAR);
		regularFont.setSize(10f);

		Paragraph companyName = new Paragraph();
		companyName.setAlignment(Align.LEFT);
		companyName.add(new TextLine(boldFont, CompanyData.YOUR_COMPANY_NAME));

		Paragraph representativeName = new Paragraph();
		representativeName.setAlignment(Align.LEFT);
		representativeName.add(new TextLine(regularFont,
				CompanyData.COMPANY_REPRESENTATIVE_NAME));

		Paragraph companyAddress = new Paragraph();
		companyAddress.setAlignment(Align.LEFT);
		companyAddress.add(new TextLine(regularFont,
				CompanyData.YOUR_COMPANY_ADDRESS));

		Paragraph taxID = new Paragraph();
		taxID.setAlignment(Align.LEFT);
		taxID.add(new TextLine(regularFont, CompanyData.TAX_ID_TITLE
				+ CompanyData.TAX_ID_VALUE));

		Paragraph phone = new Paragraph();
		phone.setAlignment(Align.LEFT);
		phone.add(new TextLine(regularFont, CompanyData.PHONE_TITLE
				+ CompanyData.PHONE_VALUE));

		Paragraph fax = new Paragraph();
		fax.setAlignment(Align.LEFT);
		fax.add(new TextLine(regularFont, CompanyData.FAX_TITLE
				+ CompanyData.FAX_VALUE));

		Text column = new Text(Arrays.asList(companyName, representativeName,
				companyAddress, taxID, phone, fax));
		column.setParagraphLeading(CompanyConst.PARAGRAPH_LEADING);

		column.setLocation(CompanyConst.X_LOCATION, CompanyConst.Y_LOCATION);

		column.setWidth(CompanyConst.WIDTH);

		column.drawOn(page);
	}

	private void drawLogo(PDF pdf, Page page) throws Exception {
		Image logo = new Image(pdf, getClass().getClassLoader()
				.getResourceAsStream(LogoData.LOGO_PATH), ImageType.PNG);

		logo.setLocation(LogoConst.X_LOCATION, LogoConst.Y_LOCATION);
		logo.scaleBy(LogoConst.SCALE_BY);
		logo.drawOn(page);

	}

	private void drawInvoiceDetails(PDF pdf, Page page) throws Exception {

		Font boldFont = new Font(pdf, FontConst.BOLD);
		boldFont.setSize(InvoiceConst.BOLD_FONT_SIZE);

		Font regularFont = new Font(pdf, FontConst.REGULAR);
		regularFont.setSize(InvoiceConst.REGULAR_FONT_SIZE);

		Table table = new Table();

		List<List<Cell>> tableData = new ArrayList<List<Cell>>();

		List<Cell> row = null;
		Cell cell = null;

		for (int rowIndex = 0; rowIndex < 4; rowIndex++) {
			row = new ArrayList<Cell>();
			for (int columnIndex = 0; columnIndex < 2; columnIndex++) {
				if (columnIndex == InvoiceConst.INVOICE_TITLE_COLUMN) {
					cell = new Cell(boldFont);
					cell.setText(InvoiceConst.titles.get(rowIndex));
					cell.setWidth(InvoiceConst.TITLE_WIDTH);
				}
				if (columnIndex == InvoiceConst.INVOICE_DATA_COULUMN) {
					cell = new Cell(regularFont);
					cell.setText(InvoiceData.values.get(rowIndex));
					cell.setWidth(InvoiceConst.DATA_WIDTH);
				}
				if (rowIndex == 0) {
					cell.setBgColor(ColorConst.GREY_BG_COLOR);
				}
				cell.setNoBorders();

				cell.setTopPadding(TableConst.CELL_TOP_PADDING - 3);
				cell.setBottomPadding(TableConst.CELL_BOTTOM_PADDING - 3);

				row.add(cell);
			}
			tableData.add(row);
		}

		table.setData(tableData);
		table.setLocation(InvoiceConst.TABLE_X_LOCATION,
				InvoiceConst.TABLE_Y_LOCATION);
		table.drawOn(page);
	}

	private void drawTable(PDF pdf, Page page) throws Exception {

		Font headerFont = new Font(pdf, FontConst.BOLD);
		Font tableFont = new Font(pdf, FontConst.REGULAR);

		headerFont.setSize(TableConst.HEADER_FONT_SIZE);
		tableFont.setSize(TableConst.TABLE_FONT_SIZE);

		Table table = new Table();

		List<List<Cell>> tableData = new ArrayList<List<Cell>>();

		List<Cell> row = null;
		Cell cell = null;

		// table header
		row = new ArrayList<Cell>();

		for (int columnIndex = 0; columnIndex < TableConst.TABLE_COLUMNS; columnIndex++) {
			cell = new Cell(headerFont);
			cell.setNoBorders();
			cell.setWidth(TableConst.columnWidth.get(columnIndex));

			cell.setTopPadding(TableConst.CELL_TOP_PADDING);
			cell.setBottomPadding(TableConst.CELL_BOTTOM_PADDING);

			// set table header text
			cell.setText(TableConst.titleTexts.get(columnIndex));

			cell.setBorder(Border.TOP, true);
			cell.setLineWidth(TableConst.HEADER_LINE_WIDTH);

			if (columnIndex == 4 || columnIndex == 5) {
				cell.setTextAlignment(Align.RIGHT);
			}

			row.add(cell);
		}
		tableData.add(row);

		// rest table cells
		for (int rowIndex = 0; rowIndex < TableConst.TABLE_ROWS; rowIndex++) {
			row = new ArrayList<Cell>();
			for (int columnIndex = 0; columnIndex < TableConst.TABLE_COLUMNS; columnIndex++) {
				cell = new Cell(tableFont);
				cell.setNoBorders();
				cell.setWidth(TableConst.columnWidth.get(columnIndex));

				cell.setTopPadding(5f);
				cell.setBottomPadding(6f);

				cell.setPenColor(ColorConst.GREY_COLOR);

				cell.setBorder(Border.TOP, true);
				cell.setLineWidth(1f);

				Random ran = new Random();
				int randomIndex = ran.nextInt(5);

				// TODO: Change randomIndex to rowIndex when will work with real
				// data
				switch (columnIndex) {
				case 0:
					Integer num = rowIndex + 1;
					cell.setText(num.toString());
					break;
				case 1:
					cell.setText(TableData.descriptions.get(randomIndex));
					break;
				case 2:
					cell.setText(TableData.qtys.get(randomIndex));
					break;
				case 3:
					cell.setText(TableData.units.get(randomIndex));
					break;
				case 4:
					cell.setText(TableData.unitPrices.get(randomIndex));
					cell.setTextAlignment(Align.RIGHT);
					break;
				case 5:
					cell.setText(TableData.totals.get(randomIndex));
					cell.setTextAlignment(Align.RIGHT);
					break;

				}
				row.add(cell);
			}
			tableData.add(row);
		}

		table.setData(tableData);
		table.setLocation(TableConst.X_LOCATION, TableConst.Y_LOCATION);
		table.drawOn(page);

	}

}
