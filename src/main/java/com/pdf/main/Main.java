package com.pdf.main;

import com.pdf.template.InvoiceTemplate;

public class Main {


	public static void main(String[] args) throws Exception {
		InvoiceTemplate invoice = new InvoiceTemplate();
		invoice.generatePdf("invoice");
		System.out.println("Done!");
	}

}
