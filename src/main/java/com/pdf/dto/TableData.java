package com.pdf.dto;

import java.util.ArrayList;
import java.util.List;

public class TableData {
	
	public static List<String> descriptions = new ArrayList<String>();
	public static List<String> qtys = new ArrayList<String>();
	public static List<String> units = new ArrayList<String>();
	public static List<String> unitPrices = new ArrayList<String>();
	public static List<String> totals = new ArrayList<String>();
	
	//Test data
	static {

		descriptions.add("[PRO] Issues time entries integration");
		descriptions.add("Language support");
		descriptions.add("[PRO] Company logo support");
		descriptions.add("[PRO] Dublicating invoices");
		descriptions.add("Invoice number format template");

		qtys.add("x5.0");
		qtys.add("x8.0");
		qtys.add("x4.0");
		qtys.add("x3.0");
		qtys.add("x1.0");
		
		units.add("hours");
		units.add("hours");
		units.add("hours");
		units.add("hours");
		units.add("hours");
		
		unitPrices.add("35.00");
		unitPrices.add("35.00");
		unitPrices.add("35.00");
		unitPrices.add("35.00");
		unitPrices.add("35.00");

		totals.add("175.00");
		totals.add("280.00");
		totals.add("140.00");
		totals.add("105.00");
		totals.add("35.00");

	}
	
}
