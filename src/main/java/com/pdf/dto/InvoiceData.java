package com.pdf.dto;

import java.util.ArrayList;
import java.util.List;

public class InvoiceData {
	
	public static String INVOICE_TITLE = "INVOICE";
	
	public static String invoiceId = "INV/20111216-27";
	public static String invoiceDate = "12/16/2011";
	public static String dueDate = "12/20/2011";
	public static String textCustomField = "Visible field in PDF";
	
	public static final List<String> values = new ArrayList<String>();
	
	static{
		values.add(invoiceId);
		values.add(invoiceDate);
		values.add(dueDate);
		values.add(textCustomField);
	}

}
