package com.pdf.dto;

public class CompanyData {
	
	public static String YOUR_COMPANY_NAME = "Your company name";
	public static String COMPANY_REPRESENTATIVE_NAME = "Company representative name";
	public static String YOUR_COMPANY_ADDRESS = "Your company address";
	public static String TAX_ID_TITLE = "Tax ID";
	public static String TAX_ID_VALUE = "";
	public static String PHONE_TITLE = "phone:";
	public static String PHONE_VALUE = "";
	public static String FAX_TITLE = "fax:";
	public static String FAX_VALUE = "";
	
	
	public static String COMPANY_INC = "Apple Inc.";
	public static String COMPANY_ADDRESS = "10500 Noth De Anza Boulevard Cupertino, CA 95014-2033";
}
