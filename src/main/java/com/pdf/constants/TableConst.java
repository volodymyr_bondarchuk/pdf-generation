package com.pdf.constants;

import java.util.ArrayList;
import java.util.List;

public class TableConst {

	public static final float HEADER_FONT_SIZE = 10f;
	public static final float TABLE_FONT_SIZE = 10f;
	
	public static final float HEADER_LINE_WIDTH = 1.4f;
	
	public static float X_LOCATION = 50f;
	public static float Y_LOCATION = 290f;
	
	public static final int TABLE_ROWS = 20;
	public static final int TABLE_COLUMNS = 6;
	
	public static final float CELL_TOP_PADDING = 5f;
	public static final float CELL_BOTTOM_PADDING = 6f;
	
	public static final List<String> titleTexts = new ArrayList<String>();
	public static final List<Float> columnWidth = new ArrayList<Float>();
	
	static {
		titleTexts.add("#");
		titleTexts.add("Description");
		titleTexts.add("Qty");
		titleTexts.add("Units");
		titleTexts.add("Unit price(AUD)");
		titleTexts.add("Total(Aud)");

		columnWidth.add(20f);
		columnWidth.add(255f);
		columnWidth.add(30f);
		columnWidth.add(40f);
		columnWidth.add(85f);
		columnWidth.add(60f);
	}
}
