package com.pdf.constants;

import java.util.ArrayList;
import java.util.List;

public class InvoiceConst {
	
	//Example
	//INVOICE
	public static float FONT_SIZE = 20f;
	public static float PARAGRAPH_LEADING = 12.0f;
	
	
	public static float X_LOCATION = 255f;
	public static float Y_LOCATION = 255f;
	
	public static float WIDTH = 200f;
//------------------------------------------------------	
	
	//Example 
//	Invoice ID: INV/20111216-27
//	Invoice date: 12/16/2011
//	Due date: 12/20/2011
//	Text custom field: Visible field in PDF
	public static float BOLD_FONT_SIZE = 11f;
	public static float REGULAR_FONT_SIZE = 11f;

	public static float TABLE_PARAGRAPH_LEADING = 12.0f;
	
	public static float TABLE_X_LOCATION = 317f;
	public static float TABLE_Y_LOCATION = 140f;
	
	public static float TITLE_WIDTH = 115f;
	public static float DATA_WIDTH = 108f;
	

	public static final int INVOICE_TITLE_COLUMN = 0;
	public static final int INVOICE_DATA_COULUMN = 1;
	
	public static final List<String> titles = new ArrayList<String>();

	static {

		titles.add("Invoice ID:");
		titles.add("Invoice date:");
		titles.add("Due date:");
		titles.add("Text custom field:");

	}
}
