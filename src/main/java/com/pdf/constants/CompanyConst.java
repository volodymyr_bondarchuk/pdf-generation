package com.pdf.constants;

public class CompanyConst {
	
	//	Your company name
	//	Company representative name
	//	Your company address
	//	Tax ID
	//	phone:
	//	fax:
	public static float FONT_SIZE = 10f;
	public static float PARAGRAPH_LEADING = 12.0f;
	
	public static float X_LOCATION = 320f;
	public static float Y_LOCATION = 50f;
	
	public static float WIDTH = 200f;

	//-------------------------------------------------
	//  POST info coordinates
	//	Apple Inc.
	//	10500 Noth De Anza Boulevard
	//	Cupertino, CA 95014-2033
	public static float POST_FONT_SIZE = 10f;
	public static float POST_PARAGRAPH_LEADING = 12.0f;
	
	public static float POST_X_LOCATION = 55f;
	public static float POST_Y_LOCATION = 150f;
	
	public static float POST_WIDTH = 150f;
}
