package com.pdf.constants.general;

public class GeneralConst {
	
	public static float HORIZONTAL_LINE_X1 = 50f;
	public static float HORIZONTAL_LINE_Y1 = 130f;
	
	public static float HORIZONTAL_LINE_X2 = 540f;
	public static float HORIZONTAL_LINE_Y2 = 130f;

	public static float HORIZONTAL_LINE_WIDTH = 1.0f;
	
	public static float DRAFT_X_LOCATION = 120f;
	public static float DRAFT_Y_LOCATION = 370f;
	
	public static int DRAFT_ROTATION = 30;
}
