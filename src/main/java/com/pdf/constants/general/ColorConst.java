package com.pdf.constants.general;

public class ColorConst {
	
	public static final int GREY_COLOR = 13684944;
	public static final int GREY_BG_COLOR = 15658734;
	
	public static final int DRAFT_COLOR = 16248815;

}
