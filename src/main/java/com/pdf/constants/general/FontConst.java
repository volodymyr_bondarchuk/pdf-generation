package com.pdf.constants.general;

import com.pdfjet.CoreFont;

public class FontConst {

	public static final CoreFont BOLD = CoreFont.HELVETICA_BOLD;
	public static final CoreFont REGULAR = CoreFont.HELVETICA;
}
