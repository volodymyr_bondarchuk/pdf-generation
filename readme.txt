Project generates a PDF file (google invoice) using pdfjet library

Requirments:

1. Java 1.7
2. Maven

How to run. There are 2 ways:
1. From CMD "mvn exec:java"
2. Import into IDE and run com.pdf.main.Main class

Project structure:
* com.pdf.constants - contains all the properties for the template (object/fileds locations, font size, color etc)
* com.pdf.dto - contains data structures. Use them to generate pdf with custom data
* com.pdf.template - markup file (like html)